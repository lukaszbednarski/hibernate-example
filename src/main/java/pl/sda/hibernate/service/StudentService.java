package pl.sda.hibernate.service;

import pl.sda.hibernate.dao.StudentDao;
import pl.sda.hibernate.model.Student;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

public class StudentService {

    private StudentDao studentDao;

    public StudentService() {
        this.studentDao = new StudentDao();
    }

    public void save(Student student){
        student.setIndex(generateIndex());

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Student>> violationSet = validator.validate(student);

        if(violationSet.isEmpty()){
            studentDao.beginTransaction();
            studentDao.save(student);
            studentDao.commitTransaction();
        } else {
          //TODO:
        }

    }

    private String generateIndex() {
        Random rnd = new Random();
        int number = rnd.nextInt(999999);

        return String.format("%06d", number);
    }

    public Optional<Student> getById(Long id){
        studentDao.beginTransaction();
        Optional<Student> student = studentDao.getById(id);
        studentDao.commitTransaction();

        return student;
    }

    public boolean doesStudentExists(Long id){
        studentDao.beginTransaction();
        boolean doesStudentExists = studentDao.doesStudentExists(id);
        studentDao.commitTransaction();

        return doesStudentExists;
    }

    public void delete (Long id) {
        studentDao.beginTransaction();
        studentDao.getById(id).ifPresent(student -> studentDao.delete(student));
//        studentDao.getById(id).ifPresent(studentDao::delete);
        studentDao.commitTransaction();
    }

    public void update(Student student){
        studentDao.beginTransaction();
        studentDao.update(student);
        studentDao.commitTransaction();
    }

    public Optional<Student> getByIndex(String index){
        studentDao.beginTransaction();
        Optional<Student> student = studentDao.getByIndex(index);
        studentDao.commitTransaction();

        return student;
    }

    public List<Student> getByName(String name){
        studentDao.beginTransaction();
        List<Student> students = studentDao.getByName(name);
        studentDao.commitTransaction();

        return students;
    }

    public List<Student> getAll(){
        studentDao.beginTransaction();
        List<Student> students = studentDao.getAll();
        studentDao.commitTransaction();

        return students;
    }

}
