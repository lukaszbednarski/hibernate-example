package pl.sda.hibernate.dao;

import pl.sda.hibernate.model.Student;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

public class StudentDao {

    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;

    public StudentDao() {
        entityManagerFactory = Persistence.createEntityManagerFactory("mysqldatabase");
    }

    public void beginTransaction() {
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
    }

    public void commitTransaction() {
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void save (Student student){
        entityManager.merge(student);
    }

    public Optional<Student> getById(Long id){
        Student student = entityManager.find(Student.class, id);
        return Optional.ofNullable(student);
    }

    public boolean doesStudentExists(Long id){
        return getById(id).isPresent();
    }

    public void delete(Student student){
        entityManager.remove(student);
    }

    public void update(Student student){
        entityManager.merge(student);
    }

    public Optional<Student> getByIndex(String index){
        String sql = "SELECT * FROM Student WHERE indeks=:student_index";
        Student student = (Student) entityManager.createNativeQuery(sql, Student.class)
                .setParameter("student_index", index)
                .getSingleResult();

        return Optional.ofNullable(student);
    }


    public List<Student> getByName(String name){
//        String sql = "SELECT * FROM Student  WHERE s.name = :student_name";

//        List<Student> students =  entityManager.createNativeQuery(sql, Student.class)
//                .setParameter("student_name", name)
//                .getResultList();

        String sql = "SELECT s FROM Student s WHERE s.name = :student_name";
        TypedQuery<Student> query =  entityManager.createQuery(sql, Student.class)
                .setParameter("student_name", name);

        return query.getResultList();
    }

    public List<Student> getAll(){
        String sql = "SELECT s FROM Student s";
        TypedQuery<Student> query =  entityManager.createQuery(sql, Student.class);

        return query.getResultList();
    }



}
