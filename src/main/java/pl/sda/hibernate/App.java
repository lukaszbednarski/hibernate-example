package pl.sda.hibernate;


import pl.sda.hibernate.model.Student;
import pl.sda.hibernate.service.StudentService;

import java.util.Optional;

public class App {
    public static void main( String[] args ) {
        StudentService studentService = new StudentService();
        studentService.save(new Student("An", "Kowalski"));

        //CASE 1
        Optional<Student> student = studentService.getById(1L);
        student.ifPresent(studentFromDb -> System.out.println(studentFromDb));

        //CASE 2
//        studentService.getById(1L).ifPresent(studentFromDb -> System.out.println(studentFromDb));
//
//        //CASE 3
//        System.out.println("Student id 1:");
//        studentService.getById(1L).ifPresent(System.out::println);
//        System.out.println("Student id 10:");
//        studentService.getById(10L).ifPresent(System.out::println);

        studentService.delete(1L);
        System.out.println("Student after deleted:");
        student.ifPresent(studentFromDb -> System.out.println(studentFromDb));
//        studentService.getById(1L).ifPresent(System.out::println);

        if (studentService.doesStudentExists(1L)){
            System.out.println("Exists");
        } else {
            System.out.println("Student doesn't exists");
        }

        studentService.getById(2L)
                .ifPresent(studentFromDb -> {
                    studentFromDb.setLastName("Nowak");
                    studentService.update(studentFromDb);
                });

//        studentService.getById(3L)
//                .map(studentFromDb -> {
//                    studentFromDb.setLastName("Nowak");
//                    return studentFromDb;
//                }).ifPresent(studentService::update);
//
        studentService.getById(3L)
                .map(studentFromDb -> {
                    studentFromDb.setLastName("Nowak");
                    return studentFromDb;
                }).ifPresent(studentFromDb -> studentService.update(studentFromDb));

        System.out.println("Student with index 960569: ");
        studentService.getByIndex("960569").ifPresent(System.out::println);

        System.out.println(studentService.getByName("Anna"));
        System.out.println(studentService.getAll());
    }
}
